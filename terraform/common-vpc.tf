# The Prod VPC
resource "aws_vpc" "prod" {
  cidr_block           = "10.84.0.0/16"
  enable_dns_hostnames = true
  tags {
    Name = "Prod VPC"
  }
}

# Subnets on this VPC (one per zone in the region)
resource "aws_subnet" "prod_a" {
  vpc_id                  = "${aws_vpc.prod.id}"
  cidr_block              = "10.84.0.0/24"
  availability_zone       = "us-west-2a"
  map_public_ip_on_launch = true
  tags {
    Name = "Prod subnet A"
  }
}
resource "aws_subnet" "prod_b" {
  vpc_id                  = "${aws_vpc.prod.id}"
  cidr_block              = "10.84.1.0/24"
  availability_zone       = "us-west-2b"
  map_public_ip_on_launch = true
  tags {
    Name = "Prod subnet B"
  }
}

resource "aws_subnet" "prod_c" {
  vpc_id                  = "${aws_vpc.prod.id}"
  cidr_block              = "10.84.2.0/24"
  availability_zone       = "us-west-2c"
  map_public_ip_on_launch = true
  tags {
    Name = "Prod subnet B"
  }
}

# The router for the VPC
resource "aws_internet_gateway" "prod" {
    vpc_id = "${aws_vpc.prod.id}"
    tags {
        Name = "Prod gateway"
    }
}

# Routing Table
resource "aws_route_table" "prod" {
    vpc_id = "${aws_vpc.prod.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.prod.id}"
    }
    tags {
        Name = "Prod routing table"
    }
}

# Route associations (one per region) for the table
resource "aws_route_table_association" "prod_a" {
    subnet_id      = "${aws_subnet.prod_a.id}"
    route_table_id = "${aws_route_table.prod.id}"
}

resource "aws_route_table_association" "prod_b" {
    subnet_id      = "${aws_subnet.prod_b.id}"
    route_table_id = "${aws_route_table.prod.id}"
}

resource "aws_route_table_association" "prod_c" {
    subnet_id      = "${aws_subnet.prod_c.id}"
    route_table_id = "${aws_route_table.prod.id}"
}
