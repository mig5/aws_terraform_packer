# An RDS instance. Note it is Multi-AZ, so it takes a while to initially provision.
resource "aws_db_instance" "prod-db" {
  identifier              = "prod-db"
  allocated_storage       = 10
  engine                  = "mysql"
  engine_version          = "${var.mysql_version}"
  instance_class          = "${var.mysql_instance_type}"
  multi_az                = true
  apply_immediately       = true
  username                = "${var.mysql_user}"
  password                = "${var.mysql_pass}"
  db_subnet_group_name    = "${aws_db_subnet_group.prod.name}"
  parameter_group_name    = "default.mysql5.6"
  backup_retention_period = 7
  vpc_security_group_ids  = ["${aws_security_group.prod_db.id}"]
  storage_type            = "gp2"
  tags {
    Name = "Prod RDS"
  }

}
resource "aws_db_subnet_group" "prod" {
  name        = "prod"
  description = "Database subnet group"
  subnet_ids  = ["${aws_subnet.prod_a.id}", "${aws_subnet.prod_b.id}", "${aws_subnet.prod_c.id}"]
  tags {
    Name = "Prod RDS Subnet group"
  }
}

# Outputs
output "db_instance_address" {
  value = "${aws_db_instance.prod-db.address}"
}
