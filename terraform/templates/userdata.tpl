#cloud-config
package_upgrade: false
runcmd:
- echo "$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).${file_system_id}.efs.${region}.amazonaws.com:/    /var/www/shared   nfs    defaults,vers=4.1 0 0" >> /etc/fstab
- mount -a
- echo "aws_access_key_id=${awscli_access_key}" >> /root/.aws/credentials
- echo "aws_secret_access_key=${awscli_secret_key}" >> /root/.aws/credentials
- while ping -c1 8.8.8.8; do aws s3 cp s3://${drupal_bucket}/drupal.tar.gz /tmp/drupal.tar.gz; break; sleep 1; done
- mkdir -p /var/www/drupal/www
- tar -xzf /tmp/drupal.tar.gz -C /var/www/drupal/www --strip-components=1
- ln -s /var/www/drupal /var/www/live.${project}.prod
- echo "$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)" > /etc/mailname
- curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O
- aws s3 cp s3://${cloudwatch_bucket}/${cloudwatch_config} /tmp/${cloudwatch_config}
- python ./awslogs-agent-setup.py -n -r ${region} -c /tmp/${cloudwatch_config}
