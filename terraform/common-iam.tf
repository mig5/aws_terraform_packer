# For fetching Drupal tarball from S3 buckets
# and also for viewing Cloudwatch logs
resource "aws_iam_user_policy" "awscli" {
    name = "awscli"
    user = "${aws_iam_user.awscli.name}"
    policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_user" "awscli" {
    name = "awscli"
    path = "/"
}

resource "aws_iam_access_key" "awscli" {
    user = "${aws_iam_user.awscli.name}"
}

output "iam_user_awscli_access_key" {
  value = "${aws_iam_access_key.awscli.id}"
}

output "iam_user_awscli_secret_key" {
  value = "${aws_iam_access_key.awscli.secret}"
}
