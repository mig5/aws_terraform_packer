# All the firewall rules (security groups) are located here.

# ELB security group
resource "aws_security_group" "prod_elb" {
 name = "prod_elb"
 description = "Rules for production elb traffic"
 vpc_id = "${aws_vpc.prod.id}"
 tags {
   Name = "Rules for production elb traffic"
 }
 # HTTP
 ingress {
   from_port = 80
   to_port = 80
   protocol = "tcp"
   cidr_blocks = [
     "0.0.0.0/0"
   ]
 }
 # HTTP
 ingress {
   from_port = 443
   to_port = 443
   protocol = "tcp"
   cidr_blocks = [
     "0.0.0.0/0"
   ]
 }
 egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

# EC2 instances (prod web traffic)
resource "aws_security_group" "prod_web" {
 name = "web_prod"
 description = "Rules for production ec2 traffic"
 vpc_id = "${aws_vpc.prod.id}"
 tags {
   Name = "Rules for production traffic"
 }
 # HTTPS
 ingress {
   from_port = 443
   to_port = 443
   protocol = "tcp"
   cidr_blocks = [
     "${aws_vpc.prod.cidr_block}"
   ]
 }
 # HTTP
 ingress {
   from_port = 80
   to_port = 80
   protocol = "tcp"
   cidr_blocks = [
     "${aws_vpc.prod.cidr_block}"
   ]
 }
 # SSH
 ingress {
   from_port = 22
   to_port = 22
   protocol = "tcp"
   cidr_blocks = [
     "${aws_vpc.prod.cidr_block}",
     "CHANGEME/32"
   ]
 }
 egress {
   from_port = 0
   to_port = 0
   protocol = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}

# DB (RDS)
resource "aws_security_group" "prod_db" {
  name        = "Prod (DB)"
  description = "Rules for production db traffic"
  vpc_id      = "${aws_vpc.prod.id}"
  tags {
    Name = "Rules for production db traffic"
  }

  ingress {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["${aws_vpc.prod.cidr_block}"]
  }

  egress {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["${aws_vpc.prod.cidr_block}"]
  }
}

# NFS (EFS)
resource "aws_security_group" "prod_efs" {
  name        = "Prod (EFS)"
  description = "Rules for production nfs traffic"
  vpc_id      = "${aws_vpc.prod.id}"
  tags {
    Name = "Rules for production nfs traffic"
  }

  ingress {
      from_port   = 0
      to_port     = 65535
      protocol    = "tcp"
      cidr_blocks = ["${aws_vpc.prod.cidr_block}"]
  }

  egress {
      from_port   = 0
      to_port     = 65535
      protocol    = "tcp"
      cidr_blocks = ["${aws_vpc.prod.cidr_block}"]
  }
}

# Elasticache
resource "aws_security_group" "prod_elasticache" {
  name        = "Prod (Elasticache)"
  description = "Rules for production elasticache traffic"
  vpc_id      = "${aws_vpc.prod.id}"
  tags {
    Name = "Rules for production elasticache traffic"
  }

  # Memcache port
  ingress {
   from_port   = 11211
   to_port     = 11211
   protocol    = "tcp"
   cidr_blocks = ["${aws_vpc.prod.cidr_block}"]
  }
}
