# SSH key for the 'admin' user in our AMI (kind of obsolete due to Packer and the 'user' Puppet module, but still)
resource "aws_key_pair" "mig5" {
  key_name   = "mig5"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBMknBQeU3w7J1FUQCbli5TQyke2HZndCzuSDIE/dXPoN9AkHeZroXu7vjdTlIA8Nenc0SOZazGswT9Ug2pUvAJog/Mw2ke2/hiJg88lTyoiwKhFa/b1CpI9T41R4ku5o2Yo5z/go5CH0JgRf2S/cXPz1DmWljV2W0LdkdAQbYDlO2PTPdnIsjq/IdpxXu2MX339x+ITKr2Pt45+Vt5dA26PFtFgQmXr+7nNdjqXQ6d1NJ0U72yMGEt0CraOPkp6ETlyCp+roHmNJlwjZ3tUt8JPxy6bQ1yBK5itLKR1HKsr0rs7Q5IQHChQ9ti+rGoNtEPvzKRSOLSKbP4tvwJfNh user@mig5"
}
