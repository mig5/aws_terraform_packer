# Credentials are read in through terraform.tfvars (not in git)
variable "access_key" {}
variable "secret_key" {}

# AWS Region
variable "region" {
  default = "us-west-2"
}

variable "availability_zones" {
  default     = "us-west-2a,us-west-2b,us-west-2c"
  description = "List of availability zones"
}

# The AMI image to use (Debian 8)
variable "aws_amis" {
  type    = "map"
  default = {
    "mig5" = "ami-CHANGEME"
  }
}

variable "ssl_certificate" {
  description = "SSL cert to add to the ELB for the HTTPS listener"
  default = "arn:aws:iam::CHANGEME:server-certificate/mig5.net"
}

variable "mysql_version" {
  description = "Version of MySQL to use"
  default = "5.6.29"
}

variable "mysql_instance_type" {  
  description = "Size of the RDS instance"
  default = "db.t2.micro"
}

variable "mysql_user" {
  description = "Name of the default MySQL admin user"
  default = "admin"
}

variable "mysql_pass" {
  description = "Password of the default MySQL admin user"
  default = "CHANGEME"
}

# Size of the EC2 instances
variable "ec2_instance_type" {
  description = "AWS instance type to use in Autoscale groups"
  default     = "t2.micro"
}

# SSH key to deploy
variable "key_name" {
  description = "Key pair to use"
  default     = "mig5"
}

# Autoscale stuff
variable "asg_min" {
  description = "Min numbers of servers in an Autoscale Group"
  default     = "3"
}

variable "asg_max" {
  description = "Max numbers of servers in an Autoscale Group"
  default     = "10"
}
