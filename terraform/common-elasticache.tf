# 3 node Elasticache cluster (one in each AZ)
resource "aws_elasticache_cluster" "prod" {
    cluster_id           = "memcache-prod"
    engine               = "memcached"
    node_type            = "cache.t2.micro"
    port                 = 11211
    num_cache_nodes      = 3
    parameter_group_name = "default.memcached1.4"
    subnet_group_name    = "${aws_elasticache_subnet_group.prod.name}"
    security_group_ids   = ["${aws_security_group.prod_elasticache.id}"]
    apply_immediately    = true
    az_mode              = "cross-az"
    availability_zones   = ["${split(",", var.availability_zones)}"]
}

resource "aws_elasticache_subnet_group" "prod" {
  name        = "prod"
  description = "Elasticache subnet group"
  subnet_ids  = ["${aws_subnet.prod_a.id}", "${aws_subnet.prod_b.id}", "${aws_subnet.prod_c.id}"]
}

# Outputs
output "elasticache_instance_cache_node0" {
  value = "${aws_elasticache_cluster.prod.cache_nodes.0.address}"
}
output "elasticache_instance_cache_node1" {
  value = "${aws_elasticache_cluster.prod.cache_nodes.1.address}"
}
output "elasticache_instance_cache_node2" {
  value = "${aws_elasticache_cluster.prod.cache_nodes.2.address}"
}
