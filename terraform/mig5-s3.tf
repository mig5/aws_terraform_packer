resource "aws_s3_bucket" "current-drupal-mig5-deployment" {
    bucket = "current-drupal-mig5-deployment"
    acl    = "private"

    tags {
        Name        = "Bucket for latest mig5 Drupal deployment"
        Environment = "Prod"
    }
}
