# Autoscaling group
resource "aws_autoscaling_group" "mig5-prod-asg" {
  name                  = "mig5-prod-asg-${aws_launch_configuration.mig5-prod-lc.name}"
  availability_zones    = ["${split(",", var.availability_zones)}"]
  max_size              = "${var.asg_max}"
  min_size              = "${var.asg_min}"
  force_delete          = true
  launch_configuration  = "${aws_launch_configuration.mig5-prod-lc.name}"
  load_balancers        = ["${aws_elb.mig5-prod-elb.name}"]
  vpc_zone_identifier   = ["${aws_subnet.prod_a.id}", "${aws_subnet.prod_b.id}", "${aws_subnet.prod_c.id}"]
  wait_for_elb_capacity = true
  tag {
    key                 = "Name"
    value               = "mig5-prod-asg"
    propagate_at_launch = "true"
  }
  lifecycle {
    create_before_destroy = true
  }
}
# cloud-init script
data "template_file" "userdata" {
    template = "${file("templates/userdata.tpl")}"
    vars {
        region            = "${var.region}"
        project           = "mig5"
        file_system_id    = "${aws_efs_file_system.prod-efs.id}"
        awscli_access_key = "${aws_iam_access_key.awscli.id}"
        awscli_secret_key = "${aws_iam_access_key.awscli.secret}"
        drupal_bucket     = "${aws_s3_bucket.current-drupal-mig5-deployment.bucket}"
        cloudwatch_bucket = "${aws_s3_bucket.mig5-cloudwatch-logs.bucket}"
        cloudwatch_config = "${aws_s3_bucket_object.cloudwatch_agent_file.key}"
    }
}
# Launch config
resource "aws_launch_configuration" "mig5-prod-lc" {
  name_prefix             = "mig5-prod-lc"
  image_id                = "${var.aws_amis["mig5"]}"
  instance_type           = "${var.ec2_instance_type}"
  security_groups         = ["${aws_security_group.prod_web.id}"]
  user_data               = "${data.template_file.userdata.rendered}"
  key_name                = "${var.key_name}"
  iam_instance_profile    = "${aws_iam_instance_profile.ec2_cloudwatch_profile.name}"
  lifecycle {
    create_before_destroy = true
  }
}

# Scaling policies
# Scale up
resource "aws_autoscaling_policy" "mig5-prod-scale-up-policy" {
  name                   = "mig5-prod-scale-up-policy"
  scaling_adjustment     = 2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.mig5-prod-asg.name}"
}

# Scale down
resource "aws_autoscaling_policy" "mig5-prod-scale-down-policy" {
  name                   = "mig5-prod-scale-down-policy"
  scaling_adjustment     = -2
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.mig5-prod-asg.name}"
}


# Metric with which the scale-up policy above applies
resource "aws_cloudwatch_metric_alarm" "mig5-prod-cloudwatch-metric-alarm-high-cpu" {
    alarm_name               = "mig5-prod-cloudwatch-metric-alarm-high-cpu"
    comparison_operator      = "GreaterThanOrEqualToThreshold"
    evaluation_periods       = "2"
    metric_name              = "CPUUtilization"
    namespace                = "AWS/EC2"
    period                   = "300"
    statistic                = "Average"
    threshold                = "80"
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.mig5-prod-asg.name}"
    }
    alarm_description        = "This metric monitors EC2 CPU utilisation in the prod cluster"
    alarm_actions            = ["${aws_autoscaling_policy.mig5-prod-scale-up-policy.arn}"]
}

# Metric with which the scale-down policy above applies
resource "aws_cloudwatch_metric_alarm" "mig5-prod-cloudwatch-metric-alarm-low-cpu" {
    alarm_name               = "mig5-prod-cloudwatch-metric-alarm-low-cpu"
    comparison_operator      = "LessThanOrEqualToThreshold"
    evaluation_periods       = "2"
    metric_name              = "CPUUtilization"
    namespace                = "AWS/EC2"
    period                   = "300"
    statistic                = "Average"
    threshold                = "40"
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.mig5-prod-asg.name}"
    }
    alarm_description        = "This metric monitors EC2 CPU utilisation in the prod cluster"
    alarm_actions            = ["${aws_autoscaling_policy.mig5-prod-scale-down-policy.arn}"]
}

output "launch_configuration" {
  value = "${aws_launch_configuration.mig5-prod-lc.id}"
}
output "asg_name" {
  value = "${aws_autoscaling_group.mig5-prod-asg.id}"
}
