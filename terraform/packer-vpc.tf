# The generic VPC for building Packer AMIs that are HVM
resource "aws_vpc" "packer" {
  cidr_block           = "10.22.0.0/24"
  enable_dns_hostnames = true
  tags {
    Name = "Packer VPC"
  }
}

# Subnets on this VPC
resource "aws_subnet" "packer" {
  vpc_id                  = "${aws_vpc.packer.id}"
  cidr_block              = "10.22.0.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "Packer subnet"
  }
}

# The router for the VPC
resource "aws_internet_gateway" "packer" {
    vpc_id = "${aws_vpc.packer.id}"
    tags {
        Name = "Packer VPC gateway"
    }
}

# Routing Table
resource "aws_route_table" "packer" {
    vpc_id = "${aws_vpc.packer.id}"
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${aws_internet_gateway.packer.id}"
    }
    tags {
        Name = "Packer routing table"
    }
}

# Route associations (one per region) for the table
resource "aws_route_table_association" "packer" {
    subnet_id      = "${aws_subnet.packer.id}"
    route_table_id = "${aws_route_table.packer.id}"
}
