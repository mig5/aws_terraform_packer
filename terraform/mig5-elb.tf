resource "aws_elb" "mig5-prod-elb" {
  name                  = "mig5-prod-elb"
  subnets               = ["${aws_subnet.prod_a.id}", "${aws_subnet.prod_b.id}", "${aws_subnet.prod_c.id}"]
  security_groups       = ["${aws_security_group.prod_elb.id}"]
  listener {
    instance_port       = 80
    instance_protocol   = "http"
    lb_port             = 80
    lb_protocol         = "http"
  }
  listener {
    instance_port       = 80
    instance_protocol   = "http"
    lb_port             = 443
    lb_protocol         = "https"
    ssl_certificate_id  = "${var.ssl_certificate}"
  }


  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 5
  }
  tags {
    Name        = "mig5-prod-elb"
    Environment = "Prod"
  }

}

output "mig5_elb_name" {
  value = "${aws_elb.mig5-prod-elb.dns_name}"
}
