Terraform manifests.
--------------------

This example is to build a 3-node autoscale cluster in us-west-2. An EFS mount is created and each EC2 instance will mount the endpoint relative to their availability zone.

An Elasticache Memcache cluster is also created, along with an RDS instance.


Initial setup
-------------

You will need to create terraform.tfvars with appropriate AWS credentials.

Note that this depends on an AMI existing (defined in common-variables.tf) which you presumably will have built with Packer (see the packer subfolder alongside this one).

However, the catch-22 is that Packer will expect a VPC to exist with which to build a t2.micro based AMI. This is handled by packer-vpc.tf.

Therefore, the best approach is:

1) Adjust variables etc to suit (see common-variables.tf, and common-keys.tf)

2) Build everything except the .tf files that begin with 'mig5' (e.g rename those files to .tf.bak)

3) Go and build the AMI with Packer, entering the VPC and Subnet IDs that were created by packer-vpc.tf

4) Add the AMI to common-variables.tf and now build the infrastructure in .tf files that begin with 'mig5' (after making any further adjustments)

A second catch-22 is that the userdata expects the EC2 instances spawned by Autoscale, to be able to fetch a Drupal tarball from an S3 bucket to 'self deploy'. The inability for it to do so will prevent the AutoScale Group from having its EC2 instances added into the ELB pool (terraform will probably hang for ages). You'll need to set this up manually the first time. Note that the tarball should contain the relevant symlinks/configuration to mount the Drupal files directory via EFS at /var/www/shared.



Cloud-init/userdata
-------------------

The cloud-init mounts the EFS mount point (handled dynamically since the mount point varies per AZ, and we don't know which AZ the EC2 instance will necessarily be in).

The EC2 instances use cloud-init userdata in the Launch Configuration to set up Cloudwatch log streaming of the Syslog and Nginx logs. They pull the cloudwatch-agent.conf from an S3 bucket after populating the /root/.aws/credentials file with the credentials of the 'awscli' user (see common-iam.tf), and installing the agent itself.

The launch configuration also is configured to expect to be able to pull your codebase down from a 'drupal.tar.gz' inside an S3 bucket 'current-drupal-mig5-deployment' and unpack it as the instances provision. There are a lot of assumptions here based on how I like to structure my codebase deployments (e.g a symlink in /var/www/live.mig5.prod pointing to the latest codebase, which I deploy separately via Jenkins/Fabric). The Packer puppet modules also contain this assumption (e.g the Nginx configuration, Drush aliases etc).

It is assumed you can deploy to whatever current cluster of EC2 instances exists, and part of this deployment involves packaging up your new build as the 'drupal.tar.gz' in said S3 bucket. In this way, future EC2 instances can ensure they pull down the 'latest' deployed codebase, without you having to do an explicit deployment as those new instances come up.

See the templates/userdata.tpl for the technical details of what happens on new EC2 instances during launch.



Autoscale
---------

The Autoscale Group interpolates the Launch Configuration name into its own name. This ensures that when you change the AMI (e.g you have updated the template), a fresh Launch Configuration will be generated, which in turn generates a new AutoScale Group. This configuration ensures that new EC2 instances are always built whenever you change the AMI.

The new instances will be added into the ELB and only when they are marked as Healthy/InService, will the old instances be removed and decommissioned.

This ensures a 'rolling' deployment model that avoids downtime or manual work to launch new instances.

See https://groups.google.com/forum/#!topic/terraform-tool/7Gdhv1OAc80 and https://robmorgan.id.au/posts/rolling-deploys-on-aws-using-terraform/ for more information on this approach.



SSL cert in ELB
---------------

Right now I haven't managed the SSL cert via Terraform. The ARN you'll see in common-variables.tf is one that I obtained from IAM after uploading the cert manually via the UI in the ELB settings. A pull request is welcome for managing an SSL cert resource for this.
