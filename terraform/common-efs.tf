# An EFS resource (with a mount target in each AZ)
resource "aws_efs_file_system" "prod-efs" {
  creation_token = "efs_prod"
  tags {
    Name = "EFS filesystem for prod"
  }
}

resource "aws_efs_mount_target" "prod-efs-subnet-a" {
  file_system_id  = "${aws_efs_file_system.prod-efs.id}"
  subnet_id       = "${aws_subnet.prod_a.id}"
  security_groups = ["${aws_security_group.prod_efs.id}"]
}

resource "aws_efs_mount_target" "prod-efs-subnet-b" {
  file_system_id  = "${aws_efs_file_system.prod-efs.id}"
  subnet_id       = "${aws_subnet.prod_b.id}"
  security_groups = ["${aws_security_group.prod_efs.id}"]
}

resource "aws_efs_mount_target" "prod-efs-subnet-c" {
  file_system_id  = "${aws_efs_file_system.prod-efs.id}"
  subnet_id       = "${aws_subnet.prod_c.id}"
  security_groups = ["${aws_security_group.prod_efs.id}"]
}

# Outputs
output "efs_instance-a" {
  value = "${aws_efs_mount_target.prod-efs-subnet-a.dns_name}"
}

output "efs_instance-b" {
  value = "${aws_efs_mount_target.prod-efs-subnet-b.dns_name}"
}

output "efs_instance-c" {
  value = "${aws_efs_mount_target.prod-efs-subnet-c.dns_name}"
}
