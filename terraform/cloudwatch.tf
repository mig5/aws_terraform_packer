resource "aws_iam_role" "ec2-cloudwatch-logs" {
    name = "ec2-cloudwatch-logs"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "ec2-cloudwatch-policy" {
    name = "ec2-cloudwatch-policy"
    description = "A policy for the EC2 cloudwatch log streaming"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams"
            ],
            "Resource": [
                "arn:aws:logs:*:*:*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::mig5-cloudwatch-logs/*"
            ]
        }
    ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "ec2-cloudwatch-logs-attach" {
    role = "${aws_iam_role.ec2-cloudwatch-logs.name}"
    policy_arn = "${aws_iam_policy.ec2-cloudwatch-policy.arn}"
}


resource "aws_iam_instance_profile" "ec2_cloudwatch_profile" {
    name = "ec2_cloudwatch_profile"
    roles = ["${aws_iam_role.ec2-cloudwatch-logs.name}"]
}

resource "aws_s3_bucket" "mig5-cloudwatch-logs" {
    bucket = "mig5-cloudwatch-logs"
    acl    = "private"

    tags {
        Name        = "Bucket for storing EC2 logs via Cloudwatch"
        Environment = "Prod"
    }
}

resource "aws_s3_bucket_object" "cloudwatch_agent_file" {
    bucket = "${aws_s3_bucket.mig5-cloudwatch-logs.bucket}"
    key = "cloudwatch-agent.conf"
    source = "files/cloudwatch-agent.conf"
    etag   = "${md5(file("files/cloudwatch-agent.conf"))}"
}

resource "aws_cloudwatch_log_group" "syslog" {
  name              = "Syslog"
  retention_in_days = "30"
}

resource "aws_cloudwatch_log_group" "nginx" {
  name              = "Nginx"
  retention_in_days = "30"
}
