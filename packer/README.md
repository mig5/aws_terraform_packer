Packer (baking our AMI for use by Terraform)
--------------------------------------------



Building the AMI
------------------

'packer build mig5.json' will build the AMI for our project. Presumably you have AWS credentials locally to facilitate this. See the docs at https://packer.io for packer options.

You will need to change the VPC and Subnet IDs in the mig5.json, and perhaps the source AMI ID too (it is based on the public Debian 8 AMI, as per https://wiki.debian.org/Cloud/AmazonEC2Image/Jessie)


Puppet modules
--------------

The Puppet modules/manifests will be used by Packer to configure the AMI to suit our needs. Lots of assumptions are baked in based on how I like to work. See the README.md inside the puppet subdirectory.
