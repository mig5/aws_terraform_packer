Puppet modules for Packer to help build our AMI.

You are likely going to want to change:

1) The path to the docroot in the 'mig5.pp' manifest
2) The drush alias in the 'drush' module
3) The SSH keys in the 'user' module
4) Perhaps the known_hosts file in the user module (currently set so that the EC2 instances can git clone from bitbucket.org safely/automatically, as the 'jenkins' user, which I use for deployments)
