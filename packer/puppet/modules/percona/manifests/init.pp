class percona() {

  # Percona client
  package { 'percona-server-client-5.6':
    ensure  => present,
    require => Exec['percona-apt-key'],
  }

  file { '/tmp/percona.deb':
    ensure => present,
    source => 'puppet:///modules/percona/percona.deb',
  }

  # Download percona apt key
  exec { 'percona-apt-key':
    command => 'dpkg -i /tmp/percona.deb',
    require => File['/tmp/percona.deb'],
    notify  => Exec['/usr/bin/apt-get update'],
  }
}
