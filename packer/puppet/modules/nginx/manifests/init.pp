class nginx($docroot='/var/www/html') {
  package { 'nginx-full':
    ensure => present,
  }

  file {
    '/var/www/shared':
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0755',
      require => Package['nginx-full'];

    '/etc/nginx/nginx.conf':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('nginx/nginx.conf.erb'),
      require => Package['nginx-full'];

    '/etc/nginx/conf.d/drupal_common_config':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/nginx/drupal_common_config',
      require => Package['nginx-full'];

    '/etc/nginx/fastcgi_params':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/nginx/fastcgi_params',
      require => Package['nginx-full'];
  }
}
