class drush() {
  file {
    '/usr/local/bin/drush':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0755',
      source  => 'puppet:///modules/drush/drush';

    '/etc/drush':
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0755';

    '/etc/drush/mig5_prod.alias.drushrc.php':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/drush/mig5_prod.alias.drushrc.php',
      require => File['/etc/drush'];
  }
}
