class php5() {
  package { [ 'php5-cgi', 'php5-cli', 'php5-curl', 'php5-gd', 'php5-ldap', 'php5-mcrypt', 'php5-memcached', 'php5-mysql', 'php5-fpm', 'imagemagick' ]:
    ensure => present,
  }

  file {
    '/etc/php5/fpm/php.ini':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/php5/php.ini',
      require => Package['php5-fpm'];

    '/etc/php5/fpm/php-fpm.conf':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/php5/php-fpm.conf',
      require => Package['php5-fpm'];

    '/etc/php5/fpm/pool.d/www.conf':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/php5/www.conf',
      require => Package['php5-fpm'];
  }
}
