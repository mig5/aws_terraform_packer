class postfix(
  $mailserver_dest_hosts = "localhost, ${::fqdn}",
  $mailserver_networks   = "localhost, localhost.localdomain, ${::ipaddress}",
  $transport_maps        = [],
  $relayhost            = '',
  $mailserver_hostname  = $::fqdn,
  ) {

  package {
    'postfix': ensure => present;
  }

  service {
    'postfix':
      ensure  => running,
      enable  => true,
      require => File['/etc/postfix'];
  }

  file {
    '/etc/postfix':
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0755',
      notify  => Service['postfix'],
      source  => 'puppet:///modules/postfix/etc/postfix',
      require => Package['postfix'];

    '/etc/postfix/main.cf':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('postfix/main.cf.erb'),
      notify  => Service['postfix'];

    '/etc/mailname':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => $mailserver_hostname,
      notify  => Service['postfix'];

    '/etc/postfix/transport':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('postfix/transport.erb'),
      require => Package['postfix'],
      notify  => Exec['/usr/sbin/postmap /etc/postfix/transport'];
  }

  exec { '/usr/sbin/postmap /etc/postfix/transport':
    refreshonly => true,
    require     => Package['postfix'],
    notify      => Service['postfix'],
  }
}
