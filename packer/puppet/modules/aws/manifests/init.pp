class aws() {
  package { 'awscli':
    ensure   => present,
    provider => pip,
    require  => Package['python-pip'],
  }

  file {
    '/root/.aws':
      ensure  => directory,
      owner   => root,
      group   => root,
      mode    => '0700',
      require => Package['awscli'];

    '/root/.aws/credentials':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0600',
      source  => 'puppet:///modules/aws/credentials',
      require => File['/root/.aws'];
  }
}
