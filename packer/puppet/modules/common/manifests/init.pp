class common($tmout='1800') {
  package { [ 'wget', 'curl', 'python-pip', 'tcpdump', 'telnet', 'mtr-tiny', 'less', 'bzip2', 'lsof', 'ntp', 'strace', 'screen', 'nfs-common', 'rsync', 'git-core' ]:
    ensure => present,
  }

  file {
    '/usr/local/bin/remove_old_builds.sh':
      ensure  => present,
      source  => 'puppet:///modules/common/remove_old_builds.sh',
      owner   => root,
      group   => root,
      mode    => '0755';

    '/usr/local/bin/pslogger':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0755',
      source  => 'puppet:///modules/common/pslogger',
      require => File['/var/log/ps'];

    '/etc/cron.d/pslogger':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/common/pslogger.cron',
      require => File['/usr/local/bin/pslogger'];

    '/var/log/ps':
      ensure => directory,
      owner  => root,
      group  => root,
      mode   => '0755';

    # Control the main /etc/bash.bashrc
    '/etc/bash.bashrc':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('common/bash.bashrc.erb');

    # Control .bashrc so we can manipulate server bash settings
    '/etc/skel/.bashrc':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => 'puppet:///modules/common/.bashrc';
  }
}
