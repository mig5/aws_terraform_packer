class user() {
  group { 'jenkins':
    ensure => present,
    gid    => 1008
  }

  user { 'jenkins':
    ensure     => present,
    home       => '/home/jenkins',
    managehome => true,
    comment    => 'Jenkins Deployer',
    gid        => 'jenkins',
    require    => Group['jenkins'],
    uid        => 1008,
    shell      => '/bin/bash',
  }

  file { 
    '/home/jenkins/.ssh':
      ensure  => directory,
      owner   => 'jenkins',
      group   => 'jenkins',
      mode    => '0700',
      require => User['jenkins'];

    '/home/jenkins/.ssh/authorized_keys':
      ensure  => 'present',
      owner   => 'jenkins',
      group   => 'jenkins',
      mode    => '0400',
      source  => 'puppet:///modules/user/authorized_keys/jenkins.pub',
      require => File['/home/jenkins/.ssh'];

    '/home/jenkins/.ssh/known_hosts':
      ensure  => present,
      source  => 'puppet:///modules/user/jenkins.known_hosts',
      owner   => jenkins,
      group   => jenkins,
      mode    => '0600',
      require => File['/home/jenkins/.ssh'];

    '/etc/sudoers.d/jenkins':
      ensure  => present,
      owner   => root,
      group   => root,
      mode    => '0600',
      content => 'jenkins ALL=(ALL) NOPASSWD: ALL',
  }
}
