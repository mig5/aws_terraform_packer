node 'default' {
  Exec { path => '/usr/bin:/usr/sbin/:/bin:/sbin' }

  exec { '/usr/bin/apt-get update':
    refreshonly => true,
  }

  # Ensure apt-get update has been run before installing any packages
  Exec['/usr/bin/apt-get update'] -> Package <| |>

  include common
  include percona
  class { 'nginx':
    docroot => '/var/www/live.mig5.prod/www'
  }
  include php5
  include postfix
  include drush
  include user
  include aws
}
