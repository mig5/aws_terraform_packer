#!/bin/bash
# determine debians release code name
CODENAME=$(lsb_release -c | awk '{print $2}')

# install puppet APT repo
wget http://apt.puppetlabs.com/puppetlabs-release-${CODENAME}.deb
sudo dpkg -i puppetlabs-release-${CODENAME}.deb
sudo apt-get -y update
# perform available upgrades
sudo apt-get -y dist-upgrade
# install puppet
sudo apt-get -y install puppet facter
