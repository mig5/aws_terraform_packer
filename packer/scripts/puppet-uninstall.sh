#!/bin/bash
# Remove puppet
CODENAME=$(lsb_release -c | awk '{print $2}')
sudo rm -f puppetlabs-release-${CODENAME}.deb
sudo apt-get -y --force-yes autoremove --purge puppet facter
